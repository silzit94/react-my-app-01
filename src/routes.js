import AboutRoutes from "./pages/About/routes";
import HomepageRoutes from "./pages/Homepage/routes";
import ShopRoutes from "./pages/Shop/routes";

const routes = [
    ...HomepageRoutes,
    ...AboutRoutes,
    ...ShopRoutes,
];

export default routes;
