import React from 'react';
import {Breadcrumb} from 'antd';

const BreadcrumbCustom = () => {
    return (
        <div className="wrapper-breadcrumb-custom">
            <Breadcrumb style={{margin: '16px 0'}}>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>List</Breadcrumb.Item>
                <Breadcrumb.Item>App</Breadcrumb.Item>
            </Breadcrumb>
        </div>
    );
}

export default BreadcrumbCustom;
