import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import {Layout, Menu} from "antd";

import menus from "../../../menus";

const {Header} = Layout;

const HeaderCustom = () => {
    const history = useHistory();
    const [defaultSelectedKeys, setDefaultSelectedKeys] = useState([]);
    const [selectedKey, setSelectedKey] = useState('`');

    useEffect(() => {
        let pathname = history.location.pathname;
        setSelectedKey(pathname);
    }, [history]);

    const handleClickMenu = ({item, key, keyPath, domEvent}) => {
        history.push(key);
        setSelectedKey(key);
    };

    return (
        <div className="wrapper-header-custom">
            <Header>
                <div className="logo"/>
                <Menu theme="dark"
                      mode="horizontal"
                      defaultSelectedKeys={defaultSelectedKeys}
                      selectedKeys={selectedKey}
                      onClick={handleClickMenu}>
                    {
                        menus.map((menu, index) => {
                            return (
                                <Menu.Item key={menu.path}>{menu.title}</Menu.Item>
                            );
                        })
                    }
                </Menu>
            </Header>
        </div>
    );
};

export default HeaderCustom;
