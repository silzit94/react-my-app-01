import React from 'react';
import { BackTop } from 'antd';
import {Layout} from "antd";

const {Footer} = Layout;

const FooterCustom = () => {
    return (
        <div className="wrapper-footer-custom">
            <BackTop visibilityHeight={100}/>
            <Footer style={{textAlign: 'center'}}>Ant Design ©2018 Created by Ant UED</Footer>
        </div>
    );
};

export default FooterCustom;
