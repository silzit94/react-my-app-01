const menus = [
    {
        title: 'Home',
        path: '/',
        exact: true,
    },
    {
        title: 'About',
        path: '/about',
        exact: true,
    },
    {
        title: 'Shop',
        path: '/shop',
        exact: true,
    },
];

export default menus;
