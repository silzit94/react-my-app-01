import React, {Suspense, lazy} from 'react';
import {
    BrowserRouter,
    Switch,
    Route,
} from 'react-router-dom';
import {Layout} from 'antd';
import './App.css';
import HeaderCustom from "./components/commons/HeaderCustom";
import FooterCustom from "./components/commons/FooterCustom";
import BreadcrumbCustom from "./components/commons/BreadcrumbCustom";
import routes from "./routes";

const Homepage = lazy(() => import('./pages/Homepage'));
const About = lazy(() => import('./pages/About'));
const Shop = lazy(() => import('./pages/Shop'));
const ProductDetail = lazy(() => import('./pages/Shop/ProductDetail'));

console.log('routes: ', routes);

const {Content} = Layout;

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <Layout className="layout" id="components-layout-demo-top">
                    <HeaderCustom/>
                    <Content style={{padding: '0 50px'}}>
                        <BreadcrumbCustom/>
                        <div className="site-layout-content">
                            <Suspense fallback={<div>Loading...</div>}>
                                <Switch>
                                    {
                                        // routes.map(({component: Component, path, ...rest}) => {
                                        //     return (
                                        //         <Route render={(props) => (
                                        //             <Component {...props} />
                                        //         )} key={path} {...rest} />
                                        //     );
                                        // })
                                    }
                                    <Route component={Homepage} path="/" exact={true}/>
                                    <Route component={About} path="/about" exact={true}/>
                                    <Route component={Shop} path="/shop" exact={true}/>
                                    <Route component={ProductDetail} path="/shop/detail/:id" exact={false}/>
                                </Switch>
                            </Suspense>
                        </div>
                    </Content>
                    <FooterCustom/>
                </Layout>
            </div>
        </BrowserRouter>
    );
}

export default App;
