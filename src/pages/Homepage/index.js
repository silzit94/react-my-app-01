import React from 'react';
import {Card} from 'antd';
import Slider from "./components/Slider";
import './styles.scss';

const Homepage = () => {
    return (
        <div className="wrapper-homepage">
            <Slider/>
            <br/>

            <div className="site-card-border-less-wrapper">
                <Card title="This is demo" bordered={false} style={{width: 300}}>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged. It was
                        popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of
                        Lorem Ipsum.
                    </p>
                </Card>
            </div>
        </div>
    );
};

export default Homepage;
