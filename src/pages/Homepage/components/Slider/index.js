import React from 'react';
import {Carousel} from 'antd';
import ImageSlider1 from '../../../../assets/images/sliders/1.jpg';
import ImageSlider2 from '../../../../assets/images/sliders/2.jpg';
import ImageSlider3 from '../../../../assets/images/sliders/3.jpg';
import ImageSlider4 from '../../../../assets/images/sliders/4.jpg';


const contentStyle = {
    height: '300px',
    color: '#fff',
    lineHeight: '250px',
    textAlign: 'center',
    background: '#364d79',
};

const imageStyle = {
    width: '100%',
    height: '600px',
    objectFit: 'fill',
};

const Slider = () => {
    return (
        <Carousel autoplay effect="fade">
            <div style={contentStyle}>
                <img src={ImageSlider1} alt="" style={imageStyle}/>
            </div>
            <div style={contentStyle}>
                <img src={ImageSlider2} alt="" style={imageStyle}/>
            </div>
            <div style={contentStyle}>
                <img src={ImageSlider3} alt="" style={imageStyle}/>
            </div>
            <div style={contentStyle}>
                <img src={ImageSlider4} alt="" style={imageStyle}/>
            </div>
        </Carousel>
    );
};

export default Slider;
