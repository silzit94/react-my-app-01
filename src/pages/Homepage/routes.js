import {lazy} from 'react';

const Homepage = lazy(() => import('../Homepage'));

const HomepageRoutes = [
    {
        path: '/',
        exact: true,
        component: Homepage,
    },
];

export default HomepageRoutes;
