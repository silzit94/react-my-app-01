import {lazy} from 'react';

const About = lazy(() => import('../About'));

const AboutRoutes = [
    {
        path: '/about',
        exact: true,
        component: About,
    },
];

export default AboutRoutes;
