import React from 'react';
import {useHistory} from "react-router-dom";
import {Card, Col, Row} from "antd";

const {Meta} = Card;

const Shop = () => {
    const history = useHistory();

    const onClickCard = (id) => {
        history.push(`/shop/detail/${id}`);
    }

    return (
        <div className="wrapper-shop">
            <h1>Shop</h1>
            <hr/>
            <div className="content-page">
                <div className="site-card-wrapper">
                    <Row gutter={16}>
                        <Col span={4} style={{paddingBottom: 20}}>
                            <Card onClick={() => onClickCard(1)}
                                  hoverable
                                  style={{width: 240}}
                                  cover={<img alt="example"
                                              src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"/>}
                            >
                                <Meta title="Europe Street beat" description="www.instagram.com"/>
                            </Card>
                        </Col>
                        <Col span={4} style={{paddingBottom: 20}}>
                            <Card
                                hoverable
                                style={{width: 240}}
                                cover={<img alt="example"
                                            src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"/>}
                            >
                                <Meta title="Europe Street beat" description="www.instagram.com"/>
                            </Card>
                        </Col>
                        <Col span={4} style={{paddingBottom: 20}}>
                            <Card
                                hoverable
                                style={{width: 240}}
                                cover={<img alt="example"
                                            src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"/>}
                            >
                                <Meta title="Europe Street beat" description="www.instagram.com"/>
                            </Card>
                        </Col>
                        <Col span={4} style={{paddingBottom: 20}}>
                            <Card
                                hoverable
                                style={{width: 240}}
                                cover={<img alt="example"
                                            src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"/>}
                            >
                                <Meta title="Europe Street beat" description="www.instagram.com"/>
                            </Card>
                        </Col>
                        <Col span={4} style={{paddingBottom: 20}}>
                            <Card
                                hoverable
                                style={{width: 240}}
                                cover={<img alt="example"
                                            src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"/>}
                            >
                                <Meta title="Europe Street beat" description="www.instagram.com"/>
                            </Card>
                        </Col>
                        <Col span={4} style={{paddingBottom: 20}}>
                            <Card
                                hoverable
                                style={{width: 240}}
                                cover={<img alt="example"
                                            src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"/>}
                            >
                                <Meta title="Europe Street beat" description="www.instagram.com"/>
                            </Card>
                        </Col>

                        <Col span={4} style={{paddingBottom: 20}}>
                            <Card
                                hoverable
                                style={{width: 240}}
                                cover={<img alt="example"
                                            src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"/>}
                            >
                                <Meta title="Europe Street beat" description="www.instagram.com"/>
                            </Card>
                        </Col>
                        <Col span={4} style={{paddingBottom: 20}}>
                            <Card
                                hoverable
                                style={{width: 240}}
                                cover={<img alt="example"
                                            src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"/>}
                            >
                                <Meta title="Europe Street beat" description="www.instagram.com"/>
                            </Card>
                        </Col>
                        <Col span={4} style={{paddingBottom: 20}}>
                            <Card
                                hoverable
                                style={{width: 240}}
                                cover={<img alt="example"
                                            src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"/>}
                            >
                                <Meta title="Europe Street beat" description="www.instagram.com"/>
                            </Card>
                        </Col>
                        <Col span={4} style={{paddingBottom: 20}}>
                            <Card
                                hoverable
                                style={{width: 240}}
                                cover={<img alt="example"
                                            src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"/>}
                            >
                                <Meta title="Europe Street beat" description="www.instagram.com"/>
                            </Card>
                        </Col>
                        <Col span={4} style={{paddingBottom: 20}}>
                            <Card
                                hoverable
                                style={{width: 240}}
                                cover={<img alt="example"
                                            src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"/>}
                            >
                                <Meta title="Europe Street beat" description="www.instagram.com"/>
                            </Card>
                        </Col>
                        <Col span={4} style={{paddingBottom: 20}}>
                            <Card
                                hoverable
                                style={{width: 240}}
                                cover={<img alt="example"
                                            src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"/>}
                            >
                                <Meta title="Europe Street beat" description="www.instagram.com"/>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>
        </div>
    );
};

export default Shop;
