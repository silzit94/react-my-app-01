import {lazy} from 'react';

const Shop = lazy(() => import('../Shop'));
const ProductDetail = lazy(() => import('../Shop/ProductDetail'));

const ShopRoutes = [
    {
        path: '/shop',
        exact: true,
        component: Shop,
    },
    {
        path: '/shop/detail/:id',
        exact: false,
        component: ProductDetail,
    },
];

export default ShopRoutes;
